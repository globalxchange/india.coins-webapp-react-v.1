import React from "react";
import nextId from "react-id-generator";
import { useHistory } from "react-router";
import { ImagesCS } from "../../assets/0a-exporter";
import AccessModal from "../Common/AccessModal/AccessModal";
import "./landing-page.style.scss";
export default function LandingPage() {
  const [modalOpen, setModalOpen] = React.useState(false);
  const handleClick = (obj) => {
    switch (obj.id) {
      case "affiliate":
        window.open("https://affiliate.app");
        break;
      case "group":
        window.open("https://inr.group");
        break;
      case "coins":
        window.open("https://teamforce.io");
        break;

      default:
        break;
    }
  };
  return (
    <div className="landing-page-main">
      {modalOpen && <AccessModal closeIt={() => setModalOpen(false)} />}
      <section className="landing-page-above">
        <div className="title-logo">
          <img src={ImagesCS.mainLogoWhole} />
        </div>
        <div className="lpm-text">
          <img src={ImagesCS.para} />
          <h6>
            We Are Building The Best Rupee To Crypto Online Media Platform.{" "}
          </h6>
          <p>Please Select One Of The Following Options</p>
        </div>
        <div className="lpm-options">
          {lists.map((obj) => (
            <div className="option-wrapper">
              <div onClick={() => handleClick(obj)}>
                <img src={obj.icon} />
              </div>
              <p>{obj.text}</p>
            </div>
          ))}
        </div>
      </section>
      <footer
        onClick={() => setModalOpen(true)}
        className="landing-page-footer"
      >
        <p>Developer Access</p>
      </footer>
    </div>
  );
}
const lists = [
  {
    keyId: nextId(),
    icon: ImagesCS.affiliateLogo,
    text: "I Want To Be A Affiliate",
    id: "affiliate",
  },
  {
    keyId: nextId(),
    icon: ImagesCS.inrGroup,
    id: "group",
    text: "I Want To Invest",
  },
  {
    keyId: nextId(),
    id: "coins",
    icon: ImagesCS.teamForce,
    text: "Work For Us",
  },
];
