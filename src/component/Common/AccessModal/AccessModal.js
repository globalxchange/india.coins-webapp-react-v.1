import React from "react";
import { useHistory } from "react-router";
import OtpInput from "react-otp-input";
import "./access-modal.style.scss";
import { ImagesCS } from "../../../assets/0a-exporter";
export default function AccessModal({ closeIt,setAccess, strict }) {
  const history = useHistory();
  const [pin, setPin] = React.useState("");
  const [error, setError] = React.useState(false)
  const handleChange = (e) => {
    setPin(e);
  };

  React.useEffect(() => {
    if (pin.length === 4) {
      if (pin == "4141") {
        history.push(`/${localStorage.getItem("country")}/trending`);
      } else {
        setError(true)
        setPin("");
      }
    }
  }, [pin]);
  return (
    <div
      onClick={() => (strict ? console.log() : closeIt())}
      className="access-modal-wrapper"
    >
      <div onClick={(e) => e.stopPropagation()} className="access-modal">
        <div className={`access-modal-body ${error?"error-otp":""}`}>
          <img src={ImagesCS.mainLogo} />
          <OtpInput
            placeholder=""
            isInputSecure={false}
            isInputNum={true}
            shouldAutoFocus={true}
            numInputs={true}
            style={{ width: 100 }}
            value={pin}
            onChange={handleChange}
            numInputs={4}
          />
        </div>
        <div
          onClick={() => {closeIt(false);
          }}
          className="access-modal-footer"
        >
          <h6>Close</h6>
        </div>
      </div>
    </div>
  );
}
