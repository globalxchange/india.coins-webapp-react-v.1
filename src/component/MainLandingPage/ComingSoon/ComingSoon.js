import React from "react";

export default function ComingSoon() {
  return (
    <div className="h-100 w-100 d-flex justify-content-center align-items-center">
      <h1 className="m-0">Coming Soon</h1>
    </div>
  );
}
