import React from "react";
import {Helmet} from 'react-helmet'
import "./companies.style.scss";
import CompanyMainCard from "./CompanyMainCard";
import CompanyRecommendationCard from "./CompanyRecommendationCard";
export default function Companies() {
  return (
    <div className="c-s-startup">
    <Helmet>
      <html lang="en" amp />
      <title itemProp="name" lang="en">
        The #1 Resource For Indian Investors Looking To Expand There Portfolio
      </title>
      <meta
        property="og:title"
        content="The #1 Resource For Indian Investors Looking To Expand There Portfolio"
      />
      <meta
        property="og:description"
        content="Access the countries best opportunities, content, and network for growing your wealth."
      />
    </Helmet>
      <div className="c-s-s-main-cards-wrapper">
        <div className="c-s-s-m-c-header">
          <h3>Canada’s Crypto Startups</h3>
        </div>
        <div className="c-s-s-m-c-body">
          {["#948A8A", "#bd4444", "#212121", "#212121", "#212121"].map(
            (obj) => (
              <CompanyMainCard color={obj} />
            )
          )}
        </div>
      </div>
      <div className="c-s-s-recommended-cards-wrapper">
        <div className="c-s-s-r-c-header">
          <h5>Recommended For Your Business</h5>
        </div>
        <div className="c-s-s-r-c-body">
          {[1, 2, 2, 2, 2].map((obj) => (
            <CompanyRecommendationCard />
          ))}
        </div>
      </div>
    </div>
  );
}
