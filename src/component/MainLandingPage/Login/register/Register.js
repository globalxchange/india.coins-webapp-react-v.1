import React from 'react'
import './register.style.scss'
export default function Register() {
    return (
        <div className="register-main">
            <div className="register-button-wrapper">
                <h1>Register</h1>
                <div>
                    <button>I Was Pre-Registered</button>
                    <button>I Got Here By Myself</button>
                    <button>I Was Referred By A Broker</button>
                </div>

            </div>
            
        </div>
    )
}
