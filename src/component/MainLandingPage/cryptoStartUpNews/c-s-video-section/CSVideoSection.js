import React from "react";
import { useHistory, useParams } from "react-router";
import { ImagesCS } from "../../../../assets/0a-exporter";
import "./cs-video-section.style.scss";
export default function CSVideoSection({
  fullSizeSidebar,
  fullScreenPlayer,
  setFullScreenPlayer,
}) {
  const { country } = useParams();
  const history = useHistory()
  return (
    <div
      className={`cs-video-section ${
        fullSizeSidebar ? "adjust-for-sidebar" : ""
      }`}
    >
      <div
        className={`cs-video-player-wrapper ${
          fullScreenPlayer ? "occupy-fullscreen" : "'"
        }`}
      >
        <div className="c-s-video-player-header">
          <h6>Video Information</h6>
          <img
            onClick={() => setFullScreenPlayer(!fullScreenPlayer)}
            src={ImagesCS.fullScreen}
          />
        </div>
        <img onClick={()=>history.push(`/${country}/video/a97fb488-458f-403d-8605-33cc43cda508`)} className="play-icon" src={ImagesCS.playIcon} />
      </div>
      <div className="cs-video-suggestion">
        {[1, 2, 3, 4].map((obj) => (
          <div className="suggestion-card">
            <div className="s-c-thumbnail"></div>
            <div className="s-c-details">
              <h5>This Country Changes </h5>
              <div>
                <p>
                  Japan Has Removed Cryptocurrency From Courts. Japan Has
                  Removed Cryptocurrency From Courts
                </p>
              </div>
              <h6>2.45 mins</h6>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
