import React, { useState, lazy, Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import MainLandingPage from "./component/MainLandingPage/MainLandingPage";
import "./App.scss";
import { CStartupProvider } from "./component/MainLandingPage/cs-context/CSContext";
import LandingPage from "./component/LandingPage/LandingPage";
function App() {
  React.useEffect(()=>{
    localStorage.setItem("country", "India");
  },[])
  return (
    <div className="App">
      <Switch>
        <Route
          
          path="/:country"
          render={() => (
            <CStartupProvider>
              <MainLandingPage />
            </CStartupProvider>
          )}
        />
        <Route path="/" component={LandingPage}/>
        <Redirect to={`/${localStorage.getItem("country")}/trending`} />
      </Switch>
    </div>
  );
}

export default App;
