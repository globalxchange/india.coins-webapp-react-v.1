import React from "react";
import Lottie from "react-lottie";
import * as animationDataLoad from "./loading.json";
import * as animationLoginLoad from "./login-loading.json";
import * as animationNoData from "./no-data.json";
import * as animationDataWorld from "./world.json";

export default function LoadingAnimationCS({
  type = "login",
  size = { height: 150, width: 150 },
  loop = true,
}) {
  let animate = null,
    animateLogin = null,
    animateWorld = null,
    animateNoData = null;
  animate = animationDataLoad.default;
  animateLogin = animationLoginLoad.default;
  animateNoData = animationNoData.default;
  animateWorld = animationDataWorld.default;

  const selectAnimation = () => {
    switch (type) {
      case "loading":
        return animate;
      case "login":
        return animateLogin;
      case "fetch":
      case "no-data":
        return animateNoData;
      case "world":
        return animateWorld;
      default:
        return;
    }
  };

  const defaultOptions = {
    loop: loop,
    autoplay: true,
    animationData: selectAnimation(),
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <React.Fragment>
      <Lottie
        style={{ padding: 0 }}
        width={size.width}
        height={size.height}
        options={defaultOptions}
      />
    </React.Fragment>
  );
}
